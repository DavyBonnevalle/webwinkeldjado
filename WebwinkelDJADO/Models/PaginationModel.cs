﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebwinkelDJADO.Models
{
    public class PaginationModel // wordt gebruikt om het aantal paginas onderaan de shop te tonen
    {
        public int TotalItems { get; set; } // total aantal te tonen producten
        public int ItemsPerPage { get; set; } // producten per pagina
        public int CurrentPage { get; set; } // de pagina die de gebruike op dat moment bekijkt

        public int TotalPages // berenkent het aantal paginas
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
        }
    }
}
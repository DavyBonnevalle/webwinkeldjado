﻿using System.Collections.Generic;
using System.Linq;


namespace WebwinkelDJADO.Models
{
    public class ShoppingCartModel
    {
        private List<ShoppingCartItemModel> items = new List<ShoppingCartItemModel>(); // creëren collection van items

        public IEnumerable<ShoppingCartItemModel> Items // return als pubkic property
        {
            get { return items; }
        }

        public void AddItem(Product product, int quantity)//item toevoegen, product en quantity meegeven
        {
            ShoppingCartItemModel item = //query van items om te z ien of item met productid al in cart zit
                items.SingleOrDefault(p => p.Product.ProductID == product.ProductID);

            if (item == null) //indien nog niet in cart is nul...
            {
                items.Add(new ShoppingCartItemModel //...wordt dan toegevoegd..
                {
                    Product = product, //..met product en hoeveelheid
                    Quantity = quantity
                });
            }
            else
            {
                item.Quantity += quantity;
            }//indien wel reeds in cart, wordt hoeveelheid items verhoogd
        }

        public void RemoveItem(int productID)
        {
            items.RemoveAll(l => l.Product.ProductID == productID);
        }//Verwijdert ALLE items met productid uit cart

        public decimal GetCartTotal()
        {
            return items.Sum(e => e.Product.Price * e.Quantity);
            // prijs  x hoeveelheid wordt teruggeven als som
        }
        public void Clear()
        {
            items.Clear();
        }

        public ShippingInfo ShippingInfo { get; set; }

        public BillingInfo BillingInfo { get; set; }

    }

}
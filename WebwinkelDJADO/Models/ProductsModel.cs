﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebwinkelDJADO.Models
{
    public class ProductsModel
    {
        public IEnumerable<Product> Products { get; set; } // een lijst van producten die in die pagina moeten getoond worden
        public PaginationModel Pagination { get; set; } // een obj met info over op welke pagina we zitten zodat we die ook onderaan kunnen toten
        public int CategoryID { get; set; } // de id van de gekozen categorie
        public string SearchString { get; set; } // de zoek term ingegeven in de search input field

        public SelectList Categories() // returnt alle categories die in de Categories tabel staan, deze wordt gebruikt om de dropdown lijst te vullen met categorieën. 
        {
            WebwinkelDJADOEntities db = new WebwinkelDJADOEntities();
            var categories = from c in db.Categories
                             orderby c.CategoryName
                             select new
                             {
                                 c.CategoryID, // de value van de categorie list item
                                 c.CategoryName, // de categorie naam die gaat getoont worden in die dropdown
                             };
            return new SelectList(categories, "CategoryID", "CategoryName");
        }
    }
}
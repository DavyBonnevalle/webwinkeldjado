﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebwinkelDJADO.Models;

namespace WebwinkelDJADO.Controllers
{
    public class ShopController : Controller
    {
        private WebwinkelDJADOEntities db = new WebwinkelDJADOEntities();
        private const int PAGE_SIZE = 3; // aantal products per pagina

        public ActionResult Index(int page = 1, int categoryID = 0, string searchString = "") // deze parameter wordt meegegeven via de Html.Actionlink, "= 1" wil zeggen dat dit een optionele parameter is (moet niet worden meegegeven) en als die niet wordt meegeeven is de default page 1. En categoryID = 0 voor de categorie. En searchString = ""
        {
            return View(GetModel(page, categoryID, searchString)); // geef een ProductsModel obj mee aan de index view
        }

        [HttpPost]
        public ActionResult Index(ProductsModel productsModel) // deze wordt gebruikt door de ShopForm in shop index (category dropdown list) wanneer een gebruiker een categorie selecteerd, submit die ShopForm naar deze ActionResult methode.
        {
            int page = 1; // reset de pagina naar de 1ste pagina van die catagorie (als ze op bv de 3 pagina stonden terwijl deze nieuwe categorie kozen worden ze nu terug verwezen naar de 1ste pagina van hun gekozen categorie)
            int categoryID = productsModel.CategoryID; // de gekozen categorie
            return View(GetModel(page, categoryID, productsModel.SearchString));
        }

        private ProductsModel GetModel(int page, int categoryID, string searchString)
        {
            var data = db.Products.Select(p => p)
                        .Where(p => categoryID == 0 || p.CategoryID == categoryID) // haalt alle producten uit de Products table waar de categoryID gelijk is aan 0 of als ze een categorie gekozen hebben, gelijk is aan die categorie.
                        .Where(p => string.IsNullOrEmpty(searchString) || p.Description.Contains(searchString)) // en als geen zoek string is opgegeven hij alle rows returnt, of waar de products description string de zoek string bevat.
                        .OrderBy(p => p.ProductName) // orderd ze adhv ProductName
                        .Skip((page - 1) * PAGE_SIZE).Take(PAGE_SIZE); // skip (sla over) een aantal items, en toon dan de items voor die pagina

            ProductsModel model = new ProductsModel
            {
                Products = data,
                Pagination = new PaginationModel
                {
                    CurrentPage = page,
                    ItemsPerPage = PAGE_SIZE,
                    TotalItems = categoryID == 0 ?
                        db.Products.Count() : // als de user geen categorie heeft gekozen tellen we alle items
                        db.Products.Select(p => p) // als de user wel een categorie heeft gekozen
                        .Where(p => p.CategoryID == categoryID)  // tellen we alle items van die categorie
                        .Where(p => p.Description.Contains(searchString)).Count() // en tellen we alle items waar die zoek string in de description staat.
                },
                CategoryID = categoryID
            };

            return model;
        }
    }
}
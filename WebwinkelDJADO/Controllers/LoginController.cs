﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebwinkelDJADO.Models;
using System.Web.Security;

namespace WebwinkelDJADO.Controllers
{
    public class LoginController : Controller
    {
        private WebwinkelDJADOEntities db = new WebwinkelDJADOEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Customer customer, string returnUrl) // gebruiker heeft de login form ingevuld, we krijgen een ingevuld Customer object binnen, en de URL die ze willen bezoeken
        {
            if (ModelState.IsValid)
            {
                int customerID;
                if (IsValid(customer.Email, customer.Password, out customerID)) 
                {
                    FormsAuthentication.SetAuthCookie(customerID.ToString(), false);

                    if (string.IsNullOrEmpty(returnUrl) ||
                        returnUrl.ToLower().Contains("login"))
                        returnUrl = Url.Action("Index", "Home");

                    if (customer.Email == "admin@webwinkeldjado.com") // al het de admin is die inlogd
                    {
                        Response.AppendCookie(new HttpCookie("User_type", "admin")); // voeg gebruikerstype toe aan cookie, om admin view (in _Layout) wel of niet te tonen
                    }
                    else
                    {
                        Response.AppendCookie(new HttpCookie("User_type", "customer"));
                    }

                    return Redirect(returnUrl);                    
                }
                else
                {
                    ModelState.AddModelError("", "The username and/or password is incorrect, please try again");
                }
            }
            return View(customer);
        }

        public bool IsValid(string Email, string Password, out int CustomerID) // derde parm is anders // gaat na of de opgegeven login gegevens juist zijn (kloppen met die in de database)
        {
            string passwordHash = SHA256.Encode(Password); // encrypteer het paswoord, op dezelfde manier als we dit dezen bij de registratie
            var data = from u in db.Customers
                       where u.Email == Email && u.Password == passwordHash
                       select new
                       {
                           u.CustomerID,
                           u.Email,
                           u.Password
                       };
            if (data.Count() > 0) // ook anders 
            {
                CustomerID = data.First().CustomerID;
                return true;
            }
            CustomerID = 0;
            return false;
        }

    }
}
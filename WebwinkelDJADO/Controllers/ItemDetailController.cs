﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebwinkelDJADO.Models;

namespace WebwinkelDJADO.Controllers
{
    public class ItemDetailController : Controller
    {
        private WebwinkelDJADOEntities db = new WebwinkelDJADOEntities();

        public ActionResult Index(int id) // parameter is de ProductID van het item (aka product) waarmee we werken, wordt meegegeven via de Html.ActionLink
        {
            var data = db.Products.SingleOrDefault(p => p.ProductID == id); // haalt één product uit de Products table waarvan de ProductID == id. Als het geen vindt return het gwn dat default product, als hij meerdere matches vindt in de tabel geeft het een error
            return View(data); // geef deze als model mee aan de index view
        }
    }
}
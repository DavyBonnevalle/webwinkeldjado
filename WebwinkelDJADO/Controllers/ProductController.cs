﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebwinkelDJADO.Code.Dal;

namespace WebwinkelDJADO.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            ProductDal dal = new ProductDal();
            ViewBag.Message = "Beheer producten";
            ViewBag.DatabaseMessage = dal.Message;
            return View(dal.ReadAll());
        }

        public ActionResult Inserting()
        {
            ViewBag.Message = "Een postcode inserten";
            return View();
        }

        [HttpPost]
        public ActionResult Insert(Models.Product product)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Message = "Insert een nieuw product in de Products tabel";
                ProductDal dal = new ProductDal(product);
                dal.Create();
                return View("Index", dal.ReadAll());
            }
            else
            {
                ModelState.AddModelError(
                    "Ongeldige gegevens", "Naam van product moet ingevuld zijn.");
                return View("Index");
            }
        }

        [HttpGet]
        // ? zorgt er voor dat een null value geen fout geeft
        public ActionResult Updating(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "Ongeldige Id";
                return View();
            }
            else
            {
                // lookup product to update
                Models.Product product = new Models.Product();
                // store Product Id
                product.ProductID = (int)id;
                ProductDal dal = new ProductDal(product);
                product = dal.ReadOne();
                ViewBag.Message = "Updating product";
                return View(product); // return de Updating view met een product obj
            }
        }

        [HttpPost]
        // ? zorgt er voor dat een null value geen fout geeft
        public ActionResult Update(Models.Product product)
        {
            if (ModelState.IsValid)
            {
                ProductDal dal = new ProductDal(product);
                dal.Update();
                ViewBag.Message = "Product is geüpdated.";
                return View("Index", dal.ReadAll());
            }
            else
            {
                ModelState.AddModelError("Ongeldige gegevens", "Zie details voor meer uitleg.");
                return View("Index");
            }
        }

        [HttpGet]
        // ? zorgt er voor dat een null value geen fout geeft
        public ActionResult Deleting(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "Ongeldige Id";
                return View();
            }
            else
            {
                // lookup product to update
                Models.Product product = new Models.Product();
                // store Product Id
                product.ProductID = (int)id;
                ProductDal dal = new ProductDal(product);
                product = dal.ReadOne();
                ViewBag.Message = "Updating product";
                return View(product); // return de Deleting view met een product obj
            }
        }

        [HttpGet]
        // ? zorgt er voor dat een null value geen fout geeft
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "Ongeldige Id";
                return View("Index");
            }
            else
            {
                // lookup product to update
                Models.Product product = new Models.Product();
                // store Product Id
                product.ProductID = (int)id;
                ProductDal dal = new ProductDal(product);
                dal.Delete();
                ViewBag.Message = "Product deleted";
                return View("Index", dal.ReadAll());
            }
        }

        [HttpGet]
        // ? zorgt er voor dat een null value geen fout geeft
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                ViewBag.Message = "Ongeldige Id";
                return View();
            }
            else
            {
                // lookup product to update
                Models.Product product = new Models.Product();
                // store Product Id
                product.ProductID = (int)id;
                ProductDal dal = new ProductDal(product);
                product = dal.ReadOne();
                ViewBag.Message = "Updating product";
                return View(product); // return de Details view met een product obj
            }
        }
    }
}
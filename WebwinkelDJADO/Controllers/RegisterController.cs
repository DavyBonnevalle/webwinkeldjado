﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebwinkelDJADO.Models;

namespace WebwinkelDJADO.Controllers
{
    public class RegisterController : Controller
    {
        private WebwinkelDJADOEntities db = new WebwinkelDJADOEntities();

        public ActionResult Index()
        {
            return View(new Customer()); // return de Index view met een leeg Customer obj
        }

        public RedirectToRouteResult Register(Customer customer) // customer heeft de register form ingevuld we krijgen hier dat ingevuld Customer object binnen
        {
            customer.Password = SHA256.Encode(customer.Password); // encrypteer het paswoord

            db.Customers.Add(customer); // voeg de nieuwe customer toe aan de Customers tabel
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using WebwinkelDJADO.Models;
using System.Data;
using System.Configuration;
/// <summary>
/// CRUD Dal class voor de Products tabel
/// </summary>
namespace WebwinkelDJADO.Code.Dal
{
    public class ProductDal : IDal<WebwinkelDJADO.Models.Product>
    {
        string message;
        WebwinkelDJADO.Models.Product product;

        public ProductDal()
        {
        }

        public ProductDal(WebwinkelDJADO.Models.Product product) // dit product object wordt meegegeven via een form submit request, na het submitten van een ingevulde form
        {
            this.product = product;
        }

        public string Message
        {
            get { return message; }
        }

        public int Create() // voeg een nieuw product toe aan de Products tabel
        {
            SqlConnection con = new SqlConnection(); // de connectie string uit Web.config
            con.ConnectionString = ConfigurationManager.ConnectionStrings["WebwinkelDJADOEntitiesADO"].ToString();
            SqlCommand cmd = new SqlCommand();
            string sqlString = "ProductsInsert"; // naam van de stored procedure
            cmd.Parameters.Add(new SqlParameter("@ProductName", SqlDbType.NVarChar, 1024)).Value = product.ProductName;
            cmd.Parameters.Add(new SqlParameter("@Price", SqlDbType.NVarChar, 1024)).Value = product.Price;
            cmd.Parameters.Add(new SqlParameter("@Image", SqlDbType.NVarChar, 1024)).Value = product.Image;
            cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar, 1024)).Value = product.Description;
            cmd.Parameters.Add(new SqlParameter("@Details", SqlDbType.NVarChar, 1024)).Value = product.Details;
            cmd.Parameters.Add(new SqlParameter("@CategoryID", SqlDbType.NVarChar, 1024)).Value = product.CategoryID;
            SqlParameter id = new SqlParameter("@ProductID", SqlDbType.Int);
            id.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(id);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sqlString;
            cmd.Connection = con;
            message = "Niets te melden";

            int result = 0; 
            using (con)
            {
                try
                {
                    con.Open();
                    result = cmd.ExecuteNonQuery(); // returnt de ProductID van de toegevoegde row
                    if ((int)id.Value == -100)
                    {
                        message = " Product bestaat al.";
                        result = -100;
                    }
                    else if (result <= 0)
                    {
                        message = " Product is niet geïnserted.";
                    }
                    else
                    {
                        message = " Product is geïnserted.";
                        result = (int)id.Value;
                    }
                }
                catch (Exception e)
                {
                    message = e.Message;
                }
            }
            return result; // 0 of de Id van de nieuwe rij
        }

        public int Delete() // verwijder een product uit de Products tabel
        {
            SqlConnection con = new SqlConnection(); // de connectie string uit Web.config
            con.ConnectionString = ConfigurationManager.ConnectionStrings["WebwinkelDJADOEntitiesADO"].ToString();
            SqlCommand cmd = new SqlCommand();
            string sqlString = "ProductsDelete"; // naam van de stored procedure
            cmd.Parameters.Add(new SqlParameter("@ProductID", SqlDbType.Int)).Value = product.ProductID;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sqlString;
            cmd.Connection = con;
            message = "Niets te melden";
            int result = 0;
            using (con)
            {
                try
                {
                    con.Open();
                    //Verbinding geslaagd
                    result = cmd.ExecuteNonQuery();
                    if (result == 0)
                    {
                        message = "Product is niet gedeleted.";
                    }
                    else
                    {
                        message = "Product is gedeleted.";
                    }
                }
                catch (SqlException e)
                {
                    message = e.Message;
                }
            }
            return result;
        }

        public List<Product> ReadAll() // retourneer alle producten uit de Products tabel
        {
            List<Product> list = new List<Product>();
            SqlConnection con = new SqlConnection(); // de connectie string uit Web.config
            con.ConnectionString = ConfigurationManager.ConnectionStrings["WebwinkelDJADOEntitiesADO"].ToString();
            SqlCommand cmd = new SqlCommand();
            string sqlString = "ProductsSelectAll"; // naam van de stored procedure
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sqlString;
            cmd.Connection = con;
            message = "Niets te melden";
            SqlDataReader result;
            using (con)
            {
                try
                {
                    con.Open();
                    // retourneert het aantal rijen dat geïnserted werd
                    message = "De database is klaar!";
                    // voeg using toe om er voor te zorgen dat
                    // de datareader gesloten wordt als we die niet
                    // meer nodig hebben
                    using (result = cmd.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // zolang dat er iets te lezen valt
                            // uit de tabel
                            while (result.Read())
                            {
                                Models.Product product = new Models.Product();
                                product.ProductID = (int)result["ProductID"];
                                product.ProductName = result["ProductName"].ToString();
                                product.Price = (decimal)result["Price"];
                                product.Image = result["Image"].ToString();
                                product.Description = result["Description"].ToString();
                                product.Details = result["Details"].ToString();
                                product.CategoryID = (int)result["CategoryID"];
                                list.Add(product);
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    message = e.Message;
                }
            }
            return list;
        }

        public Product ReadOne() // retourneer een bep product uit de Products tabel
        {
            SqlConnection con = new SqlConnection(); // de connectie string uit Web.config
            con.ConnectionString = ConfigurationManager.ConnectionStrings["WebwinkelDJADOEntitiesADO"].ToString();
            SqlCommand cmd = new SqlCommand();
            string sqlString = "ProductsSelectOne"; // naam van de stored procedure
            cmd.Parameters.Add(new SqlParameter("@ProductID", SqlDbType.Int)).Value = product.ProductID;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sqlString;
            cmd.Connection = con;
            message = "Niets te melden";
            SqlDataReader result;
            using (con)
            {
                try
                {
                    con.Open();
                    {
                        // retourneert het aantal rijen dat geïnserted werd
                        this.message = "De database is klaar!";
                        // voeg using toe om er voor te zorgen dat
                        // de datareader gesloten wordt als we die niet
                        // meer nodig hebben
                        using (result = cmd.ExecuteReader())
                        {
                            if (result.HasRows)
                            {
                                // lees de gevonden rij
                                result.Read();
                                product.ProductID = (int)result["ProductID"];
                                product.ProductName = result["ProductName"].ToString();
                                product.Price = (decimal)result["Price"];
                                product.Image = result["Image"].ToString();
                                product.Description = result["Description"].ToString();
                                product.Details = result["Details"].ToString();
                                product.CategoryID = (int)result["CategoryID"];
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return product;
        }

        public int Update() // pas een product in de Products tabel aan
        {
            SqlConnection con = new SqlConnection(); // de connectie string uit Web.config
            con.ConnectionString = ConfigurationManager.ConnectionStrings["WebwinkelDJADOEntitiesADO"].ToString();
            SqlCommand cmd = new SqlCommand();
            string sqlString = "ProductsUpdate"; // naam van de stored procedure
            cmd.Parameters.Add(new SqlParameter("@ProductID", SqlDbType.Int)).Value = product.ProductID;
            cmd.Parameters.Add(new SqlParameter("@ProductName", SqlDbType.NVarChar, 1024)).Value = product.ProductName;
            cmd.Parameters.Add(new SqlParameter("@Price", SqlDbType.Decimal)).Value = product.Price;
            cmd.Parameters.Add(new SqlParameter("@Image", SqlDbType.NVarChar, 1024)).Value = product.Image;
            cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar, 1024)).Value = product.Description;
            cmd.Parameters.Add(new SqlParameter("@Details", SqlDbType.NVarChar, 1024)).Value = product.Details;
            cmd.Parameters.Add(new SqlParameter("@CategoryID", SqlDbType.Int)).Value = product.CategoryID;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sqlString;
            cmd.Connection = con;
            message = "Niets te melden";
            int result = 0;
            using (con)
            {
                try
                {
                    con.Open();
                    //Verbinding geslaagd
                    result = cmd.ExecuteNonQuery();
                    // we moeten kijken naar de waarde van out parameter
                    // van Insert stored procedure. Als de naam van de
                    // category al bestaat, retourneert de out parameter van
                    // de stored procedure
                    // -1
                    if (result == 0)
                    {
                        this.message = " Product is niet geüpdated.";
                    }
                    else
                    {
                        this.message = " Product is geüpdated.";
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return result;
        }
    }
}
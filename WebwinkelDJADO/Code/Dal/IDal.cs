﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebwinkelDJADO.Code.Dal
{
    interface IDal<T>
    {
        string Message { get; }
        int Create();
        int Update();
        int Delete();
        T ReadOne();
        List<T> ReadAll();
    }
}